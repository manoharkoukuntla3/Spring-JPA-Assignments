package com.main;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees")
public class EmployeeResource {
   
	@Autowired
	EmployeeRepository emprepository;
	
	@GetMapping(value="/permanent")
	 List<Employee> findByemptype(){
		
		return emprepository.findByemptype();
		
	}
	@GetMapping(value="/contract")
	 List<Employee> findByemptype1(){
		
		return emprepository.findByemptype1();
		
	}
	@GetMapping(value="/count")
	int countrecords(){
		
		return emprepository.countrecords();
		
	}
	
	@GetMapping(value="/tha")
	List<Employee> LikeQuery(){
		
		return emprepository.LikeQuery();
		
	}
		

}
