package com.main;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Employee {
	
	@Id
	private Long empid;
	private String empname;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date empdoj;
	private float empsalary;
	private String emptype;
	
	public Employee() {}
	public Employee(Long empid, String empname, Date empdoj, float empsalary, String emptype) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.empdoj = empdoj;
		this.empsalary = empsalary;
		this.emptype = emptype;
	}
	public Long getEmpid() {
		return empid;
	}
	public void setEmpid(Long empid) {
		this.empid = empid;
	}
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public Date getEmpdoj() {
		return empdoj;
	}
	public void setEmpdoj(Date empdoj) {
		this.empdoj = empdoj;
	}
	public float getEmpsalary() {
		return empsalary;
	}
	public void setEmpsalary(float empsalary) {
		this.empsalary = empsalary;
	}
	public String getEmptype() {
		return emptype;
	}
	public void setEmptype(String emptype) {
		this.emptype = emptype;
	}
	

	
}
