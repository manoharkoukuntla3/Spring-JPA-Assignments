package com.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cds")
public class CdResource {
   
	@Autowired
	CdRepository cdrepository;
	
	@GetMapping(value="/all")
	public List <CD>  getAll(){
		
		return cdrepository.findAll();
		
	}
	
	@GetMapping(value="/{id}")
	public CD findBycdid(@PathVariable final Long id){
		
		return cdrepository.findBycdid(id);
		
	}
	 
	@GetMapping(value="price/{price}")
	public List<CD> findBycdid(@PathVariable final float price){
		
		return cdrepository.findBycdprice(price);
		
	}
	
	@GetMapping(value="count/{price}")
	public int findBycd(@PathVariable final float price){
		
		return cdrepository.findBycd(price);
		
	}
	
}
