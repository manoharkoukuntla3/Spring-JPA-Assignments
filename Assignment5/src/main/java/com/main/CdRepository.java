package com.main;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

@Component
public interface CdRepository extends JpaRepository<CD, Long>{

	CD findBycdid(Long id);


	  @Query(value = "SELECT * FROM CD WHERE CDPRICE >?1 ",

			    nativeQuery = true)
		
	             List<CD> findBycdprice(float price);
	

	  @Query(value = "SELECT count(*) FROM CD WHERE CDPRICE >?1 ",

			    nativeQuery = true)
		
	              int findBycd(float price);
	
	
	
	
}
