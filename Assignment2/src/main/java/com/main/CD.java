package com.main;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CD {

	@Id
	
	private Long cdid;
	private String cdtitle;
	private float cdprice;
	private String cdpublisher;
	
	public CD() { }
	
	
	public CD(Long cdid, String cdtitle, float cdprice, String cdpublisher) {
		super();
		this.cdid = cdid;
		this.cdtitle = cdtitle;
		this.cdprice = cdprice;
		this.cdpublisher = cdpublisher;
	}
	public Long getCdid() {
		return cdid;
	}
	public void setCdid(Long cdid) {
		this.cdid = cdid;
	}
	public String getCdtitle() {
		return cdtitle;
	}
	public void setCdtitle(String cdtitle) {
		this.cdtitle = cdtitle;
	}
	public float getCdprice() {
		return cdprice;
	}
	public void setCdprice(float cdprice) {
		this.cdprice = cdprice;
	}
	public String getCdpublisher() {
		return cdpublisher;
	}
	public void setCdpublisher(String cdpublisher) {
		this.cdpublisher = cdpublisher;
	}
	
	
}
