package com.main;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees")
public class EmployeeResource {
   
	@Autowired
	EmployeeRepository emprepository;
	
	@GetMapping(value="/all")
	public List <Employee>  getAll(){
		
		return emprepository.findAll();
		
	}
	
	@GetMapping(value="/{id}")
	public Employee findByempid(@PathVariable final Long id){
		
		return emprepository.findByempid(id);
		
	}
	
	@GetMapping(value="/doj/{date}")
	public List <Employee> findByempdoj(@PathVariable final Date date){
		
		return emprepository.findByempdoj(date);
		
	}
	
	@GetMapping(value="/name/{name}")
	public List <Employee> findByempnameIgnoreCase(@PathVariable final String name){
		
		return emprepository.findByempnameIgnoreCase(name);
		
	}
	 
	@PostMapping(value="/load")
	public void load(@RequestBody final Employee emp)
	{
		
		emprepository.save(emp);
	}
	
	@PutMapping(value="/update")
   public void update(@RequestBody final Employee emp)
   {
		emprepository.save(emp);
   }
	
	@RequestMapping(method=RequestMethod.DELETE , value="/delete/{id}")
	public void delete(@PathVariable final Long id)
	{
		
		emprepository.delete(id);
	}
}
