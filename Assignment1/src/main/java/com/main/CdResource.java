package com.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cds")
public class CdResource {
   
	@Autowired
	CdRepository cdrepository;
	
	@GetMapping(value="/all")
	public List <CD>  getAll(){
		
		return cdrepository.findAll();
		
	}
	
	@GetMapping(value="/{name}")
	public List <CD> findBycdpublisher(@PathVariable final String name){
		
		return cdrepository.findBycdpublisher(name);
		
	}
	 
}
